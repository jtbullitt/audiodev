# My project's README


No documentation yet. Please stand by...

```

Usage: audiodev  [ -h | -l | -L | -v | -V ] [ DEVICENAME [ -i | -I | -n | -o | -O ] ]
Prints info about audio devices (real and virtual) on this computer.
With no args, prints a list of all devices (same as '-l').
If DEVICENAME is given, prints info about only that device.
If DEVICENAME is not found, prints -1.
Options:
  -h     show this help
  -l     show device names
  -L     show detailed device info
  -n     show only the device number (-1 if device not found)
  -o     show only the number of output channels (-1 if device not found)
  -I     show the ordinal input number of this device (-2 if device has no inputs)
  -O     show the ordinal output number of this device (-2 if device has no outputs)
  -v     show the version number of this program
  -V     show the version and copyright info of this program
Options -i and -o print an 'ordinal' device number, such that:
    the N'th device possessing inputs has 'ordinal input number' N.
    the N'th device possessing outputs has 'ordinal output number' N.
	
```
