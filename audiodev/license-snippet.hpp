R"****(
audiodev - a macOS command line utility to get info about connected audio devices
Copyright (C) 2019  JT Bullitt
License: GNU GPLv3
Contact: http://www.jtbullitt.com/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Additionally, some portions of this software are governed by other licenses:
    * libportaudio.a
    * portaudio.h
        Copyright (c) 1999-2002 Ross Bencina and Phil Burk
        Latest version available at: http://www.portaudio.com/

)****"
