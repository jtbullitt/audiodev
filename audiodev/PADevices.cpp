/*
 audiodev - a command line tool to get info about connected audio devices
 Copyright (C) 2019  JT Bullitt
 For contact info: http://www.jtbullitt.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 The code in this file is based on PortAudio's pa_devs.c example file.
 See the license in portaudio.h.
 */

//
//  PADevices.cpp
//  audioDevices
//
//  Created by jtb on 5/22/17.

#include "PADevices.hpp"


/*
 Constructor
 */
PADevices::PADevices() {
	PaError err = Pa_Initialize();
	if( err != paNoError ) {
		printf( "ERROR: Pa_Initialize returned 0x%x\n", err );
		return;
	}

	int numDevices = Pa_GetDeviceCount();
	if( numDevices < 0 ) {
		printf( "ERROR: Pa_GetDeviceCount returned 0x%x\n", numDevices );
		return;
	}

	// collect the device info
    int defaultDeviceInputIndex = Pa_GetDefaultInputDevice(); // the default output device
    int defaultDeviceOutputIndex = Pa_GetDefaultOutputDevice(); // the default output device
	PADevice *device;
    int nInputDevice=0;
    int nOutputDevice=0;
	for( int i=0; i<numDevices; i++ ) {
		const PaDeviceInfo *deviceInfo = Pa_GetDeviceInfo(i);
        device = new PADevice(deviceInfo, i);
        if (device->hasInput()) {
            nInputDevice++;
            device->ordinalInput(nInputDevice);
        }
        if (device->hasOutput()) {
            nOutputDevice++;
            device->ordinalOutput(nOutputDevice);
        }

		// if this device is a default input or output, then flag it as such
		device->setDefaultInput(i == defaultDeviceInputIndex);
        device->setDefaultOutput(i == defaultDeviceOutputIndex);

		_deviceList.push_back(device);
	}
}


/*
Destructor
 */
PADevices::~PADevices() {
	;
}

/*
 	Returns a pointer to the device of the given name.
 	Returns nullptr if no scuh device is found.
 */
PADevice * PADevices::Get(std::string name) const {
	PADevice *device;
	
	// if we're looking for an output device named "default", then return the default output device
	if (name == "default") {
		return _deviceList[Pa_GetDefaultOutputDevice()];
	}
	
	for( int i=0; i<_deviceList.size(); i++ ) {
		device = _deviceList[i];
		if (device->name() == name) {
			return device;
		}
	}
	return nullptr;
}

/*
 Returns a pointer to the device of the given index.
 Returns nullptr if no scuh device is found.
 */
PADevice * PADevices::Get(int index) const {
	if (index < 0 || index >= _deviceList.size()) {
		return nullptr;
	}
	else {
		return _deviceList[index];
	}
}

/*
 Print a list of the devices
*/
void PADevices :: Summarize() const {
    if (_deviceList.size()) {
        _deviceList[0]->SummaryTitle();
    }
    
    for (auto i=0; i < _deviceList.size(); i++) {
        _deviceList[i]->Summarize();
    }
}

/*
 Print the details of all the devices found
 */
void PADevices ::Details() const {
	printf( "PortAudio version: 0x%08X\n", Pa_GetVersion());
//	printf( "Version text: '%s'\n", Pa_GetVersionInfo()->versionText );
	int numDevices = Pa_GetDeviceCount();
	if( numDevices < 0 ) {
		printf( "ERROR: Pa_GetDeviceCount returned 0x%x\n", numDevices );
		return;
	}
	printf( "Number of devices = %d\n", numDevices );
	
	for (int i=0;i< _deviceList.size(); i++) {
		printf("\n");
		_deviceList[i]->Details();
	}
}


