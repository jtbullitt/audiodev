/*
 audiodev - a command line tool to get info about connected audio devices
 Copyright (C) 2019  JT Bullitt
 For contact info: http://www.jtbullitt.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//
//  PADevice.hpp
//  audioDevices
//
//  Created by jtb on 5/22/17.
//

#ifndef PADevice_hpp
#define PADevice_hpp

#include <iostream>
#include <math.h>
#include "/opt/local/include/portaudio.h"
#include <vector>

#define PA_ERR_DEVICE_NOT_FOUND -1
#define PA_ERR_DEVICE_HAS_NO_INPUTS -2
#define PA_ERR_DEVICE_HAS_NO_OUTPUTS -2

/*
 An extension of the PaDeviceInfo struct...

 */

class PADevice {
public:
	PADevice(const PaDeviceInfo *, int);
	~PADevice();
	void Details() const;
    void Summarize() const;
    void SummaryTitle() const;
	void PrintSupportedStandardSampleRates(const PaStreamParameters *,
											const PaStreamParameters *) const;
	
	void setDefaultInput(bool isDefault) {_isDefaultInput = isDefault;};
	bool isDefaultInput() {return _isDefaultInput;};
	void setDefaultOutput(bool isDefault) {_isDefaultOutput = isDefault;};
	bool isDefaultOutput() {return _isDefaultOutput;};

	// getters for the struct parameters (see portaudio.h)
	int structVersion() const {return _deviceInfo->structVersion;};
	std::string name() const {return _deviceInfo->name;};
	PaHostApiIndex hostApi() const {return _deviceInfo->hostApi;};
	int maxInputChannels() const {return _deviceInfo->maxInputChannels;};
	int maxOutputChannels() const {return _deviceInfo->maxOutputChannels;};
	PaTime defaultLowInputLatency() const {return _deviceInfo->defaultLowInputLatency;};
	PaTime defaultLowOutputLatency() const {return _deviceInfo->defaultLowOutputLatency;};
	PaTime defaultHighInputLatency() const {return _deviceInfo->defaultHighInputLatency;};
	PaTime defaultHighOutputLatency() const {return _deviceInfo->defaultHighOutputLatency;};
	double defaultSampleRate() const {return _deviceInfo->defaultSampleRate;};
	int index() const {return _index;};
    
    bool hasOutput() const { return (_deviceInfo->maxOutputChannels > 0); };
    bool hasInput() const { return (_deviceInfo->maxInputChannels > 0); };
    
    void ordinalOutput(int n) {_ordinalOutput = n;}; // this device is the n'th one possessing outputs
    int ordinalOutput() const {return _ordinalOutput;}; // returns n if this is the n'th device with outputs
    void ordinalInput(int n) {_ordinalInput = n;}; // this device is the n'th one possessing inputs
    int ordinalInput() const {return _ordinalInput;}; // returns n if this is the n'th device with inputs


private:
	PaDeviceInfo * _deviceInfo;	// the PADeviceInfo struct
	int _index;	// this device's index into the system's list of audio devices
	bool _isDefaultInput; //TRUE if this is the system's default input device
	bool _isDefaultOutput; //TRUE if this is the system's default input device
    int _ordinalOutput;
    int _ordinalInput;
};


#endif /* PADevice_hpp */
