/*
 audiodev - a command line tool to get info about connected audio devices
 Copyright (C) 2019  JT Bullitt
 For contact info: http://www.jtbullitt.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//
//  PADevice.cpp
//  audioDevices
//
//  Created by jtb on 5/22/17.
//

#include "PADevice.hpp"
#include <sstream> // stringstream

/*
 Constructor
 */
PADevice::PADevice( const PaDeviceInfo * dev, int index) {
	
	_index = index; // this device's index into the system's list of audio devices
	
	_deviceInfo = new PaDeviceInfo;	// allocate space 
	_deviceInfo->structVersion = dev->structVersion;
	_deviceInfo->name = dev->name;
	_deviceInfo->hostApi = dev->hostApi;
	_deviceInfo->maxInputChannels = dev->maxInputChannels;
	_deviceInfo->maxOutputChannels = dev->maxOutputChannels;
	_deviceInfo->defaultLowInputLatency = dev->defaultLowInputLatency;
	_deviceInfo->defaultLowOutputLatency = dev->defaultLowOutputLatency;
	_deviceInfo->defaultHighInputLatency = dev->defaultHighInputLatency;
	_deviceInfo->defaultHighOutputLatency = dev->defaultHighOutputLatency;
	_deviceInfo->defaultSampleRate = dev->defaultSampleRate;
	_isDefaultInput = false;
	_isDefaultOutput = false;
    _ordinalOutput = PA_ERR_DEVICE_HAS_NO_OUTPUTS;
    _ordinalInput = PA_ERR_DEVICE_HAS_NO_INPUTS;
}


/*
 Destructor
 */
PADevice::~PADevice() {
}

/*
 Print a tidy summary of this device
 */
void PADevice::Summarize() const {
    char s[256];
    std::string summaryField("%-30s %3d %5s %5s %5d %5d %s");
    char ordIn[4]="-";
    if (_ordinalInput >= 0) { sprintf(ordIn,"%d",_ordinalInput);}
    char ordOut[4]="-";
    if (_ordinalOutput >= 0) { sprintf(ordOut,"%d",_ordinalOutput);}
    std::string comment = "";
    if (_isDefaultOutput) comment="← default output";
    if (_isDefaultInput) comment="← default input";
    snprintf(s, 256, summaryField.c_str(),
            _deviceInfo->name,
             _index,
             ordIn,
             ordOut,
            _deviceInfo->maxInputChannels,
            _deviceInfo->maxOutputChannels,
            comment.c_str()
			 );
	std::cout << s << std::endl;
}

void PADevice::SummaryTitle() const {
    char s[256];
    std::string summaryFieldTitle("%-28s %5s %5s %5s %5s %5s");
    snprintf(s, 256, summaryFieldTitle.c_str(),
            "NAME",
            "#",
             "#I",
             "#O",
            "INS",
            "OUTS");
    std::cout << s << std::endl;
}

/*
 Print all the gory details of this device
 */
void PADevice::Details() const {
	
	std::string comment = "";
    if (_isDefaultOutput) comment="(default output)";
    if (_isDefaultInput) comment="(default input)";
    printf( "Device name                 : %s %s\n", _deviceInfo->name, comment.c_str() );
	printf( "Index                       : %d\n", _index );
	printf( "Host API                    : %s\n",  Pa_GetHostApiInfo( _deviceInfo->hostApi )->name );
	printf( "Max inputs                  : %d\n", _deviceInfo->maxInputChannels  );
	printf( "Max outputs                 : %d\n", _deviceInfo->maxOutputChannels  );
	
	printf( "Default low input latency   : %8.4f\n", _deviceInfo->defaultLowInputLatency  );
	printf( "Default low output latency  : %8.4f\n", _deviceInfo->defaultLowOutputLatency  );
	printf( "Default high input latency  : %8.4f\n", _deviceInfo->defaultHighInputLatency  );
	printf( "Default high output latency : %8.4f\n", _deviceInfo->defaultHighOutputLatency  );
	
	printf( "Default sample rate         : %8.2f\n", _deviceInfo->defaultSampleRate );
	
	/* poll for standard sample rates */
	PaStreamParameters inputParameters, outputParameters;
//	inputParameters.device = i;
	inputParameters.channelCount = _deviceInfo->maxInputChannels;
	inputParameters.sampleFormat = paInt16;
	inputParameters.suggestedLatency = 0; /* ignored by Pa_IsFormatSupported() */
	inputParameters.hostApiSpecificStreamInfo = NULL;
	
//	outputParameters.device = i;
	outputParameters.channelCount = _deviceInfo->maxOutputChannels;
	outputParameters.sampleFormat = paInt16;
	outputParameters.suggestedLatency = 0; /* ignored by Pa_IsFormatSupported() */
	outputParameters.hostApiSpecificStreamInfo = NULL;
	
	if( inputParameters.channelCount > 0 )
	{
		printf("Supported standard sample rates for half-duplex 16 bit %d channel input : ",
			   inputParameters.channelCount );
		PrintSupportedStandardSampleRates( &inputParameters, NULL );
	}
	
	if( outputParameters.channelCount > 0 )
	{
		printf("Supported standard sample rates for half-duplex 16 bit %d channel output : ",
			   outputParameters.channelCount );
		PrintSupportedStandardSampleRates( NULL, &outputParameters );
	}
	
	if( inputParameters.channelCount > 0 && outputParameters.channelCount > 0 )
	{
		printf("Supported standard sample rates for full-duplex 16 bit %d channel input, %d channel output : ",
			   inputParameters.channelCount, outputParameters.channelCount );
		PrintSupportedStandardSampleRates( &inputParameters, &outputParameters );
	}
}

void PADevice::PrintSupportedStandardSampleRates(
											  const PaStreamParameters *inputParameters,
											  const PaStreamParameters *outputParameters ) const
{
	static double standardSampleRates[] = {
		8000.0, 9600.0, 11025.0, 12000.0, 16000.0, 22050.0, 24000.0, 32000.0,
		44100.0, 48000.0, 88200.0, 96000.0, 192000.0, -1 /* negative terminated  list */
	};
	int     i, printCount;
	PaError err;
	
	printCount = 0;
	for( i=0; standardSampleRates[i] > 0; i++ )
	{
		err = Pa_IsFormatSupported( inputParameters, outputParameters, standardSampleRates[i] );
		if( err == paFormatIsSupported )
		{
			if( printCount == 0 )
			{
				printf( "\t%8.2f", standardSampleRates[i] );
				printCount = 1;
			}
			else if( printCount == 4 )
			{
				printf( ",\n\t%8.2f", standardSampleRates[i] );
				printCount = 1;
			}
			else
			{
				printf( ", %8.2f", standardSampleRates[i] );
				++printCount;
			}
		}
	}
	if( !printCount )
		printf( "None\n" );
	else
		printf( "\n" );
}
