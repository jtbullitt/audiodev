/*
 audiodev - a command line tool to get info about connected audio devices
 Copyright (C) 2019  JT Bullitt
 For contact info: http://www.jtbullitt.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//
//  PADevices.hpp
//  audioDevices
//
//  Created by jtb on 5/22/17.

#ifndef PADevices_hpp
#define PADevices_hpp

#include <stdio.h>
#include <vector>
#include <math.h>
#include <libgen.h>  // basename()
#include "PADevice.hpp"
#include "/opt/local/include/portaudio.h"

/*
 A collection of audio devices (PADevice)
 */


class PADevices {
public:
	PADevices();
	~PADevices();
	uint64_t Count() { return _deviceList.size();};
    void Details() const;
    void Summarize() const;

	PADevice * Get(std::string) const;
	PADevice * Get(int) const;
	

	
private:
	std::vector<PADevice *> _deviceList;
};


#endif /* PADevices_hpp */
