/*
 audiodev - a command line tool to get info about connected audio devices
 Copyright (C) 2019  JT Bullitt
 License: GNU GPLv3
 Contact: http://www.jtbullitt.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//
//  main.cpp
//  audioDevices
//
//  Created by jtb on 5/21/17.
//

#define VERSION_PROGRAM_NAME "audiodev"
#define VERSION_MAJOR 0 // change only if the build's API is NOT backward-compatible with previous builds.
#define VERSION_MINOR 1 //change when adding backward-compatible features.
#define VERSION_PATCH_FORMAT "%Y%m%d.%H%M" // a time stamp that reflects backward-compatible bug fixes (patches).

// exit codes
// These are, in general, different from the error codes defined in PADevices.hpp, which may be negative.
#define ERR_BAD_USAGE 1
#define ERR_DEVICE_NOT_FOUND 2


#include <iostream>
#include <fstream>
#include <vector>
#include "PADevices.hpp"

void ExitWithUsage(std::string , int, bool);
std::string Copyright();
std::string Version();

int main(int argc, const char * argv[]) {
    
    // Let's keep the cmdline arg parsing simple to avoid the complexity of Boost::.
    // The first switch() simply detects possible options (i.e., args that begin with a '-');
    // The second switch() interprets the options and decides what to do with them.
    // See ExitWithUsage() for the meaning of the options.
    std::string option(""), deviceToLookup("");
	bool isVerbose=true; // false suppresses printouts
	
	// convert the args to an easier-to-handle vector
	std::vector<std::string> args;
	for (auto i=0; i < argc; i++) {
		if (!strcmp(argv[i],"-q")) {
			isVerbose=false;
			continue; // don't save this arg; we're done with it
		}
		args.push_back(argv[i]); // save this arg
	}
	
	

	switch (args.size()) {
		case 1:
            option = "-l"; // by default, with no args, invoke the '-l' option (print a summary list of all devices)
			break;
			
		case 2: // e.g., "audiodev -l" or "audiodev DEVICENAME"
            if (args[1][0] == '-') {
                option = args[1];
            } else {
                deviceToLookup = args[1];
                option = "-L";
            }
			break;

		case 3:
            if (args[1][0] == '-' && args[2][0] != '-') {  // e.g., "audiodev -L DEVICENAME"
                option = args[1];
                deviceToLookup = args[2];
            } else
            if (args[1][0] != '-' && args[2][0] == '-') {  // e.g., "audiodev DEVICENAME -L"
                option = args[2];
                deviceToLookup = args[1];
            } else {
                if (isVerbose) std::cout << "*** Too many options given (" << args[1] << " " << args[2] << "). Only one allowed." << std::endl;
                ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            
			break;

		default: // unexpected number of arguments
			ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
			break;
	}
    
    // Options must contain exactly two chars: a hyphen, followed by a char
    if (option.size() > 2) {
        if (isVerbose) std::cout << "*** Unrecognized option: " << option << std::endl;
		ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
    }
    
    PADevices devices;
    PADevice *device = devices.Get(deviceToLookup); // look up the requested device (if any)
    
    // Now handle the options.
    // The option always starts with a '-'. From now on, just use the single character that follows the hyphen.
    switch (option[1]) {
        case 'L': // detailed List
            if (deviceToLookup != "") { // if a specific device was given
                if (device) { // it it was found
                    if (isVerbose) device->Details(); // print full report
                } else { // device not found
                    if (isVerbose) std::cout << "Device '" + deviceToLookup + "' not found." << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
                }
            } else {
                if (isVerbose) devices.Details(); // report info on all devices
            }
            break;
            
        case 'l': // summary list
            if (deviceToLookup != "") { // if a specific device was given
                if (device) { // it it was found
                    if (isVerbose) device->SummaryTitle(); // print just a summary
                    if (isVerbose) device->Summarize();
                } else { // device not found
                    if (isVerbose) std::cout << "Device '" + deviceToLookup + "' not found." << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
                }
            } else {
                if (isVerbose) devices.Summarize(); // report info on all devices
            }
            break;
            
        case 'o': // output channel count (number of output channels)
            if (deviceToLookup != "") { // if a specific device was given
               if (device) {
                    if (isVerbose) std::cout << device->maxOutputChannels() << std::endl;
                } else {
                    if (isVerbose) std::cout << PA_ERR_DEVICE_NOT_FOUND << std::endl; // this may be negative
					exit(ERR_DEVICE_NOT_FOUND); // this is
             }
            } else { // this option only applies to a specific device
				ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            break;
            
        case 'i': // input channel count (number of input channels)
            if (deviceToLookup != "") { // if a specific device was given
                if (device) {
                    if (isVerbose) std::cout << device->maxInputChannels() << std::endl;
                } else {
                    if (isVerbose) std::cout << PA_ERR_DEVICE_NOT_FOUND << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
               }
            } else { // this option only applies to a specific device
				ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            break;

        case 'n': // audio device number
            if (deviceToLookup != "") { // if a specific device was given
                if (device) {
                    if (isVerbose) std::cout << device->index() << std::endl;
                } else {
                    if (isVerbose) std::cout << PA_ERR_DEVICE_NOT_FOUND << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
               }
            } else { // this option only applies to a specific device
				ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            break;

        case 'I': // ordinal input device number
            if (deviceToLookup != "") { // if a specific device was given
                if (device) {
                    if (isVerbose) std::cout << device->ordinalInput() << std::endl;
                } else {
                    if (isVerbose) std::cout << PA_ERR_DEVICE_NOT_FOUND << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
               }
            } else { // this option only applies to a specific device
				ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            break;

        case 'O': // ordinal output device number
            if (deviceToLookup != "") { // if a specific device was given
                if (device) {
                    if (isVerbose) std::cout << device->ordinalOutput() << std::endl;
                } else {
                    if (isVerbose) std::cout << PA_ERR_DEVICE_NOT_FOUND << std::endl;
					exit(ERR_DEVICE_NOT_FOUND);
               }
            } else { // this option only applies to a specific device
				ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            }
            break;

		case 'h': // help
		case 'H': // help
			ExitWithUsage(args[0], 0, isVerbose);
            break;
            
        case 'v': // version  info
            if (isVerbose) std::cout << Version() << std::endl;
            break;
 
        case 'V': // version and copyright info
            if (isVerbose) std::cout << Version() << std::endl;
            if (isVerbose) std::cout << Copyright() << std::endl;
            break;

        default: // unrecognized option
            if (isVerbose) std::cout << "*** Unrecognized option: " << option << std::endl;
			ExitWithUsage(args[0], ERR_BAD_USAGE, isVerbose);
            break;
    }
    
}

/*
 Copyright() -- return the contents of the license-snippet.hpp file
 */
std::string Copyright() {
    // use C++11's raw string literal feature to extract the text (see https://stackoverflow.com/a/25021520/5011245)
    std::string text(
#include "license-snippet.hpp"
    );
    return text;
}


/*
 Version() -- construct and return a plausible version number for this build
 */
std::string Version() {
    // get the XCode build date/time
    char tmp[128];
    sprintf(tmp,"%s%s",__DATE__,__TIME__);

    // parse it into a tm struct
    struct tm tm;
    strptime(tmp, "%b %d %Y%H:%M:%S", &tm);

    // parse the tm struct back into a friendly date/time string for the "patch" number
    char patch[128];
    strftime(patch, sizeof(patch), VERSION_PATCH_FORMAT, &tm);

    char version[128];
    sprintf(version,"%s %d.%03d.%s", VERSION_PROGRAM_NAME, VERSION_MAJOR, VERSION_MINOR, patch);
    return version;
}


void ExitWithUsage(std::string appName, int exitVal, bool isVerbose) {
	if (isVerbose) {
		appName = basename((char*)(appName.c_str()));
		std::string u ="Usage: ";
		u += appName + "  [ -h | -l | -L | -v | -V ] [-q] [ DEVICENAME [ -i | -I | -n | -o | -O ] ]\n";
		u += "Prints info about audio devices (real and virtual) on this computer.\n";
		u += "With no args, prints a list of all devices (same as '-l').\n";
		u += "If DEVICENAME is given, prints info about only that device.\n";
		u += "If DEVICENAME is not found, prints -1.\n";
		u += "Options:\n";
		u += "  -h     show this help\n";
		u += "  -l     show device names\n";
		u += "  -L     show detailed device info\n";
		u += "  -n     show only the device number (-1 if device not found)\n";
		u += "  -o     show only the number of output channels (-1 if device not found)\n";
		u += "  -q     quiet (print no output)\n";
		u += "  -I     show the ordinal input number of this device (-2 if device has no inputs)\n";
		u += "  -O     show the ordinal output number of this device (-2 if device has no outputs)\n";
		u += "  -v     show the version number of this program\n";
		u += "  -V     show the version and copyright info of this program\n";
		u += "Options -i and -o print an 'ordinal' device number, such that:\n";
		u += "    the N'th device possessing inputs has 'ordinal input number' N.\n";
		u += "    the N'th device possessing outputs has 'ordinal output number' N.\n";
		std::cout << u << std::endl;
	}
    exit(exitVal);
}



